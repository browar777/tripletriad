const DB = {
    login: "mo1285_Email",
    password: "FLWtC2SEKzuPHwG6ZeM7",
    IP: "128.204.218.115:27017"
}

module.exports = {

    port: process.env.PORT || 3000,

    getDbConnectionString: () =>{
        return 'mongodb://'+DB.login+':'+ DB.password +'@'+ DB.IP +'/'+DB.login;
    },

    headers(req, res, next){
         // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();
    }
}