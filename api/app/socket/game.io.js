const socketIo = require('socket.io');
const Game = require('../game');

module.exports = class GameIo{
    constructor(server){
        this.gameIo = socketIo( server ).of('/game');
        this.games = [];

        this.middleware();
        this.onConnection();

    }

    middleware(){
        this.gameIo.use(function(socket, next){

            if (socket.handshake.query.room || socket.handshake.query.cardLvl )
                return next();

            next(new Error('Missing room hash'));
        });
    }

    onConnection(){
        this.gameIo.on('connection', (socket) => {

            var room = socket.handshake.query.room;
            var cardLvl = socket.handshake.query.cardLvl;


           if( this.games[room] ){
                if( this.gameIo.adapter.rooms[room].length < 2 )
                    this.games[room].addPlayer(socket);
                else
                    console.log( `room ${room} is full` );

            }else{
                this.games[room] = new Game(room, cardLvl, this.gameIo);
                this.games[room].addPlayer(socket, true);

            }



            this.onDisconnect(socket, room);
        });
    }//END onConnection


    onDisconnect(socket, room){
        socket.on('disconnect', (s) => {

            if( this.games[room] ){
                this.games[room].removePlayer(socket.id);
                console.log(   " disconnected " + socket.id);

                if( typeof this.gameIo.adapter.rooms[room] == 'undefined' ){
                  console.log("delete empty room");
                  delete this.games[room];
                }

            }

        });

    }//END onDisconnect

}//EMD class
