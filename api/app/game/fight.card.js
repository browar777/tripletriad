module.exports = function(y, x, card, board, socketId){

  const switchOwner = []
  const grid = [];

  grid[0] = cellExist( board, y-1, x );
  grid[1] = cellExist( board, y, x+1 );
  grid[2] = cellExist( board, y+1, x );
  grid[3] = cellExist( board, y, x-1 );

  for (let i = 0; i < grid.length; i++) {

    if( !grid[i].card )
      continue;

    let num = (i < 2)? i+2: i-2;

    if( card.stats[i] > grid[i].card.stats[num] ){
      switchOwner.push( grid[i].position );
      grid[i].socketId = socketId;
    }
  }

  return switchOwner;
}//END function

function cellExist(board, y, x){

  try{
    if(  board[y][x]  ){
      board[y][x].position = {y,x};
      return board[y][x];
    }
    else
        return  {card: null};
  }
  catch(e){
    return {card: null};
  }


}
