const randomizeCard = require('./random.cards');

module.exports = class Player{
  constructor(socket,host){
    this.socket = socket;
    this.host = host;
    this.restartGame = false;
    this.cards;
    this.cardLvl;
  }

  giveCards(cardLvl){
    this.cardLvl = cardLvl;
    this.cards = randomizeCard(cardLvl);
  }

  sendCards(msg){
    this.socket.emit('cards',{
      cards: this.cards,
      socketId: this.socket.id,
      host: this.host,
      msg: msg
    });
  }

  sendBoard(board){
    this.socket.emit('board',{
      board: board
    });
  }

  resetPlayer(newBoard){
    this.restartGame = false;
    this.giveCards(this.cardLvl);
    this.sendCards("NEXT GAME");
    this.sendBoard( newBoard );
  }

}
