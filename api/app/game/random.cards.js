const Fs = require('fs');
const StatsCard = require('./stats.cards');

module.exports = function(cardLevel){

    try{
        var cards = Fs.readdirSync(__dirname + '/../../public/cards/lvl'+cardLevel);
    }catch(e){
        console.log("unnow card category: "+ cardLevel);
        return null;
    }


    var handCard = [];

    for(let i = 0; i < 5; i++){
        let randomKey = Math.floor(Math.random()*cards.length);
        handCard.push(  StatsCard(cards[randomKey])  );

        cards.splice(randomKey, 1);
    }

    return handCard;
}//end randomizeCard
