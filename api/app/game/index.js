const Player = require('./player');
const randomizeCard = require('./random.cards');
const CardFight = require('./fight.card');

const boardCel = {
    card: null,
    socketId: null
}

//emit to one player //gameIo.to(socketId).emit


module.exports = class Game {
    constructor( room, cardLvl, gameIo ){
        this.gameIo = gameIo;
        this.room = room;
        this.cardLvl = cardLvl;
        this.players = [];
        this.board = this.createBoard();
        this.countPlacedCard = 0;

    }

    addPlayer(socket, host = false){

       var msg = host ? 'GAME CREATE' : 'YOU JOIN ROOM';
        //Join player to room
        socket.join(this.room);


        var player = new Player(socket, host);
        player.giveCards(this.cardLvl);
        player.sendCards(msg);
        player.sendBoard( this.board );
        this.players[socket.id] = player;


        this.onPlaceCardOnBoard(socket);
        this.onRestartGame(socket);

    }//END addPlayer

    onPlaceCardOnBoard(socket){
        socket.on('place_card', res => {

            //check if player have emited card
            const handCardIndex = this.players[socket.id].cards.findIndex( (hand) => {
                return hand.img === res.card.img
            });

            if( handCardIndex < 0 && this.board[res.y][res.x].card != null  ){
                console.log('card dont exist or board cell is already full');
                return false;
            }

            this.board[res.y][res.x].card = res.card;
            this.board[res.y][res.x].socketId = socket.id;

            this.players[socket.id].cards.splice(handCardIndex, 1);

            this.emitNewCardOnBoard(res.y, res.x, res.card, socket.id);

            this.countPlacedCard++;
            if( this.countPlacedCard > 8 )
              this.endGame();

         });
    }

    emitNewCardOnBoard(y ,x, card, socketId){

      const newCard = {
        position: {
          y,
          x
        },
        card,
        socketId
      };

      const switchCard = CardFight( y, x, card, this.board, socketId);


      this.gameIo.in(this.room).emit( 'place_card', {
        newCard,
        switchCard
      });

    }


    createBoard(){
        var board = [];
        for(let i = 0;i < 3;i++){
            board[i] = [];
            for(let z = 0; z < 3; z++){
                board[i][z] = {
                    card: null,
                    socketId: null
                };
            }
        }
        return board;
    }//end createBoard



    removePlayer(socketId){
        delete this.players[socketId];
    }

    endGame(){

      const playersPoints = [];

      for(let y = 0;y < 3;y++){
        for(let x = 0; x < 3; x++){

            if( playersPoints[ this.board[y][x].socketId ] )
              playersPoints[ this.board[y][x].socketId ]++
            else
              playersPoints[ this.board[y][x].socketId ] = 1
        }
      }

      for( let socketId in playersPoints )
        if( playersPoints[socketId] > 4 )
            this.gameIo.in(this.room).emit( 'game_end', {
              winner: socketId
            });

    }


    onRestartGame(socket){

      socket.on('restart_game', res => {
        this.players[socket.id].restartGame = res;
        let resetCounter = 0;

        for( let socketId in this.players ){

          if( socketId != socket.id &&  res == true ){
            this.gameIo.to(socketId).emit( 'reset_check', {
              reset: true
            });
          }

          if( this.players[socketId].restartGame )
            resetCounter++
        }


        if(resetCounter > 1){
          this.board = this.createBoard();
          this.countPlacedCard = 0;

          for( let socketId in this.players ){
             this.players[socketId].resetPlayer(this.board);
          }

        }


      });
    }//END onRestartGame




}//ENd class



