module.exports = function(card){

  const stats = card.split('_');

  return {
    img: card,
    stats: stats[1].split('').map( val => (val == 'A')? 10 : parseInt(val) ),
    power: stats[2].split('.')[0]
  }
}
