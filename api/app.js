const express = require( 'express' );
const http = require('http');
const mongoose = require( 'mongoose' );
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

//const Game = require('./app/game');


 const Config = require('./app/config');
 const Routes = require('./app/routes');
 const GameIo = require('./app/socket/game.io');


// mongoose.connect( config.getDbConnectionString() , { useMongoClient: true });

const app = express();

app.use( Config.headers );
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use( express.static(__dirname + '/public') );
app.use('/',Routes);


const server = http.Server( app );
server.listen(process.env.PORT || 3000);

new GameIo( server );
