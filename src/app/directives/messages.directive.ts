import { Directive, ElementRef, Input ,HostListener } from '@angular/core';


@Directive({
  selector: '[appMessages]',
  host: {
    '[style.-webkit-text-fill-color]': '"#000000"',
    '[style.-webkit-text-stroke-color]': '"#ffffff"',
    '[style.-webkit-text-stroke-width]': '"2px"',
    '[style.textAlign]': '"center"',
    '[style.font-size]': '"150px"',
    '[style.font-family]': '"ff"',
    '[style.position]': '"absolute"',
    '[style.margin]': '"auto"',
    '[style.top]': '"0"',
    '[style.bottom]': '"0"',
    '[style.left]': '"0"',
    '[style.right]': '"0"',
    '[style.height]': '"fit-content"',
    '[style.width]': '"100%"',
    '[style.overflow]': '"hidden"',
    '[style.display]': '"none"'
  }
})
export class MessagesDirective {

  @Input() public appMessages: any;

  private delay: number;

  constructor(public el: ElementRef) {

  }


  @HostListener('change') ngOnChanges(): void {

    if( this.appMessages == '' )
      return;


      var spans = [];
      this.delay = 1000 / this.appMessages.length;
      this.el.nativeElement.innerHTML = '';

      for(let i = 0; i < this.appMessages.length; i++){

        var char = this.appMessages[i] == ' ' ? "\u00A0" : this.appMessages[i]

        var span = document.createElement("span");
        span.style.transform = 'translateX(1215px)';
        span.style.display = 'inline-block';
        span.style.transition = 'all 1.5s cubic-bezier(0, 0.09, 0.63, 1.4)';
        var t = document.createTextNode(char);
        span.appendChild(t);

        spans.push(span);

        this.el.nativeElement.appendChild(span);
      }//end For

    this.animateLetter(spans);

  }

  ngOnInit() {}

   public async animateLetter(spans){

    this.showElement();

    for(let i = 0; i < spans.length; i++){
      await this.timeout(spans[i],'translateX(0px)',this.delay);
    }

    await this.timeout(null,'',3000);

    for(let i = spans.length-1; i >= 0; i--){
      await this.timeout(spans[i],'translateX(1200px)',this.delay);
    }

    this.hiddeElement();

  }

  public timeout(span = null, translate = '', ms): Promise<any> {
    return new Promise(resolve => setTimeout(() => {

      if( span == null )
        return resolve(false);

      span.style.transform = translate;
      return resolve(true);
    }, ms));
  }


  public hiddeElement(): void{
    this.el.nativeElement.style.display = 'none';
  }

  public showElement(){
    this.el.nativeElement.style.display = 'inline-block';
  }


}

/*

  const container = document.getElementById('text');
            const text = container.innerText;
            container.innerHTML = '';
            const spans = [];

          //  var span = '';
            for(let i = 0; i < text.length; i++){

                var char = text[i] == ' ' ? "\u00A0" : text[i]

                var span = document.createElement("span");
                var t = document.createTextNode(char);
                span.appendChild(t);

                spans.push(span);



                container.appendChild(span);



            }

            var delay = 500;

            spans.forEach(element => {
                setTimeout( ()=>{
                    element.classList.add('move');
                },delay );
                delay += 500;
            });


*/
