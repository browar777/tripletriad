import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GameService } from '../../services/sockets/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public lvls: any[] = new Array(10);
  public cardLevel: any;

  constructor(public router: Router, public _gameService: GameService) { }

  ngOnInit() {
  }

  public startGame(): void {
    const hash = Math.random().toString(36).substring(4);

    this.router.navigate(['game/' + hash]);
  }




}
