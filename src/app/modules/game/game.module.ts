import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MessagesDirective } from '../../directives/messages.directive';

import { GameComponent } from './game.component';
import { ResetGameComponent } from './reset-game/reset-game.component';


const routes: Routes = [
  { path: '' , component: GameComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  declarations: [GameComponent, MessagesDirective, ResetGameComponent],
  entryComponents: [ ResetGameComponent ],
  providers: []
})
export class GameModule { }


