import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HandCardAnimations } from '../../animations/cards.animations';
import { environment } from '../../../environments/environment';

import { GameService } from '../../services/sockets/game.service';
import { ResetGameComponent } from './reset-game/reset-game.component';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
  animations: [ HandCardAnimations ]
})
export class GameComponent implements OnInit, OnDestroy {

  @ViewChild('modal',{ read: ViewContainerRef }) resetModal: ViewContainerRef;


  public cards: any[] = [];
  public board: any[][] = [];
  public socketId = null;
  public selectedCardIndex: number;
  public cardsAssets = environment.cards;
  public lastPlayerRound: string = null;
  public message: string = '';

  constructor(public _gameService: GameService,  private route: ActivatedRoute, private router: Router, private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit() {
    const hash = this.route.snapshot.paramMap.get('hash');

    this._gameService.connect(hash).subscribe(res => {
    this.socketId = res.socketId;
    this.message = res.msg;
    this.lastPlayerRound = null;
    this.cards = res.cards.map(card => {
       return {
         card: card,
         imgUrl:`url(${this.cardsAssets}lvl${this._gameService.cardLvl}/${card.img})`
        }
     });

     //host have first move
     if( res.host )
        this.lastPlayerRound = res.socketId;
    });

    this.newBoard();


    this.playerMove();
    this.endGame();

  }

  newBoard(): void{
    this._gameService.onNewBoard().subscribe( res => {
      this.board = res.board;
        console.log("set new board", res);
        //remove model if game is reset
        this.removeResetModel();
    });
  }

  playerMove(): void{
    this._gameService.onPlaceCardOnBoard().subscribe( res => {

      res.newCard.card.img = `url(${this.cardsAssets}lvl${this._gameService.cardLvl}/${res.newCard.card.img})`;


     this.board[  res.newCard.position.y ][  res.newCard.position.x ] = res.newCard;
     this.lastPlayerRound = res.newCard.socketId;

     if( res.switchCard.length > 0){
       for(let pos of res.switchCard){
         this.board[pos.y][pos.x].socketId = res.newCard.socketId;
       }
     }

   });

  }


  endGame(): void{

    this._gameService.onEndGame().subscribe(res => {
      if( res.winner == this.socketId )
        this.message = "YOU WIN";
      else
        this.message = "YOU LOST";

      this.showResetModel();
    });

  }

  ngOnDestroy(): void {
    this._gameService.disconnect();
  }

  /**
   * Select Cart that will be place on board
   * @param id
   */
  public selectCard(id: number) {
    if( this.lastPlayerRound != this.socketId )
      this.selectedCardIndex = id;
    else
      alert("Not yours round");
  }

  public placeCard( y: number, x: number) {

    if ( this.selectedCardIndex == null || this.board[y][x].card != null ) {
     // console.log( this.board[y][x] );
      return;
    }

    const card = this.cards[this.selectedCardIndex].card;

    // this.board[y][x].card = card;

    this.cards.splice(this.selectedCardIndex, 1);
    this.selectedCardIndex = null;

    this._gameService.emitPlaceCard(y, x, card);

  }

  public test(  ) {


  }

  showResetModel(data = null){
    let componentFactory = this.componentFactoryResolver
                                .resolveComponentFactory(ResetGameComponent);
     let componentRef = this.resetModal.createComponent(componentFactory);
     componentRef.instance.data = data;
  }

  removeResetModel(){
    this.resetModal.clear();
  }


}// END class

