import { Component, OnInit, Input } from '@angular/core';
import { GameService } from '../../../services/sockets/game.service';

@Component({
  selector: 'app-reset-game',
  templateUrl: './reset-game.component.html',
  styleUrls: ['./reset-game.component.css']
})
export class ResetGameComponent implements OnInit {

  @Input() data;
  public check: boolean = false;

  constructor(public _gameService: GameService) { }

  ngOnInit() {
    console.log("Data send when model is created" ,this.data );

    this._gameService.onResetCheck().subscribe(res=>{
      if(res.reset == true)
        this.check = true;
    });

  }

  resetGame(reset: boolean){
    this._gameService.emitResetGame(reset);
  }

}
