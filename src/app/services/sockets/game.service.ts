import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as Socket from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  public socket: Socket;
  public cardLvl: number = 1;

  constructor() { }

  public connect(room: string): Observable<any> {

    // Connect to socket
    this.socket = Socket( 'localhost:3000/game' , {
      query: {
        room: room,
        cardLvl: this.cardLvl
      }
    });

    // Get connection error
    this.socket.on('error', function(err) {
      if (err === 'Invalid namespace') {
        console.warn('Attempted to connect to invalid namespace');
      } else {
        console.warn('Error on socket.io client', err);
      }
    });

    return new Observable( observ => {
      this.socket.on('cards', res => observ.next(res) );
      });

  }// END connect

  public emitPlaceCard(y: number, x: number, card: string) {

    this.socket.emit('place_card', {y, x, card });

  }// END emitPlaceCard

  public emitResetGame(reset){
    this.socket.emit('restart_game', reset);
  }

  public disconnect(): void {
    this.socket.disconnect();
    console.log('socket disconected');
  }

  public onPlaceCardOnBoard(): Observable<any> {
    return new Observable( observ => {
        this.socket.on('place_card', res => observ.next(res) );
    });
  }

  public onEndGame(): Observable<any>{
    return new Observable( observ => {
        this.socket.on('game_end', res => observ.next(res) );
    });
  }

  public onNewBoard(): Observable<any>{
    return new Observable( observ => {
        this.socket.on('board', res => observ.next(res) );
    });
  }

  public onResetCheck(): Observable<any>{
    return new Observable( observ => {
        this.socket.on('reset_check', res => observ.next(res) );
    });
  }

  public test(x) {
    this.socket.emit('test', x);
    this.socket.emit('emit_to_all', null);
  }




}
