import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, ExtraOptions } from '@angular/router';

import { HomeComponent } from './modules/home/home.component';
import { GameModule } from './modules/game/game.module';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'game/:hash', loadChildren: () => GameModule}


   // {path: 'login', loadChildren: 'app/modules/routing/login/login.module#LoginModule'},
    //{path: '', redirectTo: '/dashboard', pathMatch: 'full'},
   // { path: 'dashboard', loadChildren: 'app/modules/routing/dashboard/dashboard.module#DashboardModule',  canActivate: [ AuthGuard ] },
   // { path: 'exam/:hash', loadChildren: 'app/modules/routing/exam/exam.module#ExamModule' },
    // { path: '', component: HomepageComponent },
    // { path: 'test', component: TestComponent },
    // { path: 'order', loadChildren: 'app/modules/order/order.module#OrderModule' },
    // { path: 'search', loadChildren: 'app/modules/search/search.module#SearchModule' },
    // { path: 'dashboard', loadChildren: 'app/modules/dashboard/dashboard.module#DashboardModule' },
    //{ path: '**', loadChildren: 'app/modules/routing/page-not-found/page-not-found.module#PageNotFoundModule' }
];

const routerOptions: ExtraOptions = { 
    preloadingStrategy: PreloadAllModules,  
    useHash: true  
};

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [  ]
})

export class AppRouting {
    public static readonly components = [HomeComponent];
    public static readonly entry = [];
}