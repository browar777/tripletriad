import {
    trigger,
    state,
    style,
    animate,
    transition,
    query,
    stagger,
    keyframes
  } from '@angular/animations';


  export const HandCardAnimations = trigger('HandCardAnimations', [
    transition('* => *', [ // each time the binding value changes

      query(':enter', [
        style({transform: 'translateY(-900px) scale(1.5)'}),
        stagger(100, [
          animate(300, style({transform: 'translateY(0px) scale(1)'}) )
        ])
      ] ,{optional: true}),

      query(':leave', [
        stagger(100, [
          animate( 300,  style({marginTop: '-209px', transform: 'translateY(-900px) translateX(-50%) scale(1.5)'}) )
        ])
      ],{optional: true}),

    ])

  ]);


    // trigger('flyInOut', [
 
    //   transition('void => *', [ stagger( style({transform: 'translateY(-800px) scale(1.5)'}),
    //     animate('0.4s ease-out'))
       
    //   ]),
    //   transition('* => void', [ 
    //     animate(400, style({transform: 'translateY(-800px) scale(1.5)'}))
    //   ])
    // ])



    // trigger('XXX', [
    //   transition('* => *', [ // each time the binding value changes

    //     query(':enter', [
    //       style({transform: 'translateY(-900px) scale(1.5)'}),
    //       stagger(100, [
    //         animate(300, style({transform: 'translateY(0px) scale(1)'}) )
    //       ])
    //     ] ,{optional: true}),


    //     query(':leave', [
    //       stagger(100, [
    //         animate( 400,  style({transform: 'translateY(-900px) translateX(-50%) scale(1.5)'}) )
    //       ])
    //     ],{optional: true}),

        
    //     query('div', [
    //       stagger(100, [
    //         animate( 200,  style({marginTop: '-209px'}) )
    //       ]),
    //     ],{optional: true}),

       
    //   ])

    // ]),

    // trigger('RemoveCardFromHand', [
    //   transition( '*  => void', [ 
    //     animate( 1500, 
    //       style({transform: 'translateY(-900px) translateX(-50%) scale(1.5)'}),
    //       //style({marginTop: '-209px', transform: 'translateY(-900px) translateX(-50%) scale(1.5)'})
    //     ) 
    //   ])
    // ]),

    //  trigger('CardReposition', [
    //   transition( '*  => void', [ 
  
    //     query(':leave', [
    //      // style({transform: 'translateY(-900px) scale(1.5)'}),
    //       stagger(500, [
    //         animate('1s', style({marginTop: '-209px'}) )
    //       ])
    //     ])

    //   ])
    // ])