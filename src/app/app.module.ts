import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppRouting } from './app.routing';

import { GameService } from './services/sockets/game.service';






@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
class AppComponent {
}


@NgModule({
  declarations: [
    AppComponent,
    AppRouting.components
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRouting,
    FormsModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }





